require('dotenv').config();

const _ = require("lodash")
const express = require("express");
const openapi = require('@wesleytodd/openapi');
const SentimentAnalysis = require('./controller/getSentiment');
const KeywordAnalysis = require('./controller/getKeywords');
const GeneralData = require('./data/_analyzed.json')

const docs = require('./docs/OpenAPI.json')
const app = express();
const port = 3000;

const oapi = openapi({
    openapi: '3.0.0',
    info: {
        title: 'TwoHat ML Analysis',
        description: 'Machine Learning analysis with a Third Party API built with Express. This is a simple API that adds value on top of the data already provided.',
        version: '1.0.0'
    }
})

app.use(oapi)
app.use(express.json());
app.use('/docs', oapi.swaggerui)

app.get("/sentiments", oapi.path(docs.sentiments), async (req, res) => {
    var data = _.map(GeneralData, _.partialRight(_.pick, ['original.text', 'original.player', 'original.client_id', 'sentiment.response']));
    return res.json(data).status(200);
});

app.get("/keywords", oapi.path(docs.keywords), async (req, res) => {
    var data = _.map(GeneralData, _.partialRight(_.pick, ['original.text', 'original.player', 'original.client_id', 'keywords.response']));
    return res.json(data).status(200);
});

app.post("/sentiment", oapi.path(docs.sentiment), async (req, res) => {
    const data = await SentimentAnalysis(req.body.sentence)
    return res.json(data.response).status(200);
});

app.post("/keyword", oapi.path(docs.keyword), async (req, res) => {
   const data = await KeywordAnalysis(req.body.sentence)
  return res.json(data.response).status(200);
});

app.listen(port, () => {
  console.log(`http://localhost:${port}`);
});
