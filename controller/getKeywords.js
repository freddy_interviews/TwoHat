require("dotenv").config();
const axios = require("axios");

async function KeywordAnalysis(text) {
  try {
    const { data } = await axios.post(
      process.env.ENDPOINT + "/keyword",
      {
        text: text,
        language: "en",
      },
      {
        headers: {
          Authorization: `Bearer ${process.env.API_KEY}`,
          "Content-Type": "application/json",
        },
      }
    );
    return data;
  } catch (e) {
    return [];
  }
}

module.exports = KeywordAnalysis;
