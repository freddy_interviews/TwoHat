const fs = require("fs");

(function () {
  let data = fs.readFileSync("./raw.json", {
    encoding: "utf8",
    flag: "r",
  });
  data = data.split("\n").join(",");
  fs.writeFileSync("./_parsed.json", `[${data}]`);
})();
