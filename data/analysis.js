let data = require('./_parsed.json');
const SentimentAnalysis = require('../controller/getSentiment');
const KeywordAnalysis = require('../controller/getKeywords');
const fs = require('fs');

(async function () {
    data = data.slice(0, 50);
    const request = await Promise.all(
        data.map(async item => {
            const sentiment = await SentimentAnalysis(item.text)
            const keywords = await KeywordAnalysis(item.text)
            return { 
                original: item,
                sentiment: sentiment,
                keywords: keywords
            } 
            
        })
    );

    console.log(request)
    fs.writeFileSync("./_analyzed.json", JSON.stringify(request));
})();
